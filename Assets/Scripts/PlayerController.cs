﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.AI;

namespace AssemblyCSharp
{
	public class PlayerController : MonoBehaviour {

		private float health = 100.0f;
		private bool is_dead = false;
		private int weapon = 30;
		public Text healt_text;
		public Text balas_text;
		public GameObject[] enemies;
		private MapDoom mapDoom;
		// Use this for initialization
		void Start () {
			this.mapDoom = new MapDoom ();

			this.mapDoom.CreateMap01 ();
			GameObject.Find("FPSController").transform.position = this.mapDoom.getPosition();
			GameObject.Find("ZombieRig").GetComponent<NavMeshAgent>().Warp(this.mapDoom.getZombiRigPosition());
			GameObject.Find("FirstPersonCharacter").transform.eulerAngles = this.mapDoom.getRotation();
		}
		void OnCollisionEnter (Collision col)
		{
			Debug.Log (col.gameObject.tag);
			if(col.gameObject.tag == "Enemy")
			{
				Debug.Log ("COLUSION");
				health -= 10.0f;
				if (health < 0) {
					healt_text.text = "Vida: " + health + "%";
					SceneManager.LoadScene("Menu");
				}
			}
		}	// Update is called once per frame
		void Update () {
			if (Input.GetMouseButtonDown (0)) {
				Debug.Log ("dISPARO");
				if (weapon > 0) {
					weapon -= 1;
					balas_text.text = weapon + "/30";
					foreach(GameObject enemy in enemies)
					{
						if (!(enemy.activeSelf))
							continue;
						int success = (int)((enemy.transform.position.x - transform.position.x) / Camera.main.transform.forward.x);
						int success2 = (int)((enemy.transform.position.y - transform.position.y)/ Camera.main.transform.forward.y);
						int success3 = (int)((enemy.transform.position.z - transform.position.z)/ Camera.main.transform.forward.z);
						int o = (success - success3);
						if ((success == success3)) {
							enemy.GetComponent< AssemblyCSharp.EnemyController >().health -= 10f;
						}
					}
				}
			}
			foreach (GameObject enemy in enemies) {
				if (!(enemy.activeSelf))
					continue;
				int in_x = (int)(enemy.transform.position.x - transform.position.x);
				int in_y = (int)(enemy.transform.position.y - transform.position.y);
				int in_z = (int)(enemy.transform.position.z - transform.position.z);
				if ((-1 < in_x) && (in_x < 1) &&(-1< in_z) && (in_z < 1)){
					health -= 1.0f;
					healt_text.text = "Vida: " + health + "%";
					if (health < 0) {
						SceneManager.LoadScene("MainMenu");
						break;
					}
				}
			}
			if (Input.GetKeyDown (KeyCode.R)) {
				Debug.Log ("SE APRETO R");
				weapon = 30;
				balas_text.text = weapon + "/30";
			}
		}
	}

}