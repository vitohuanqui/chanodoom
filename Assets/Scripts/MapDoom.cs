﻿using System;

using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.AI;

namespace AssemblyCSharp
{
	public class NodeDoom
	{
		public class EdgeDoom
		{
			public const int TYPE_NORMAL = 0;
			public const int TYPE_TELEPORT = 1;
			public int type;
			public NodeDoom node;
			public EdgeDoom(NodeDoom node, int type)
			{
				this.type = type;
				this.node = node;
			}
		}
		public int codeBoss = 0;
		public int posX = 0;
		public int posY = 0;
		private ArrayList nextNodes;
		public GameObject gameObject;
		public NodeDoom(float level,int posX, int posY, int codeBoss)
		{
			this.codeBoss = codeBoss;
			this.posX = posX;
			this.posY = posY;
			this.nextNodes = new ArrayList ();
			//this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
			this.gameObject = GameObject.Find (String.Format ("Cube_{0}_{1}", posX, posY));
			if (this.gameObject) {
				//this.gameObject.name = String.Format ("Cube_{0}_{1}", posX, posY);
				//this.gameObject.transform.position = new Vector3 (posX, (level * 0.5f) - 2.5f, posY);
				//this.gameObject.transform.localScale = new Vector3 (1.0f, level + 3, 1.0f);
				MeshRenderer gameObjectRenderer = this.gameObject.GetComponent<MeshRenderer> ();
				if (level < 1.0) {
				}
				if (this.codeBoss != 0) {
					Color whateverColor = new Color(1,0,0,1);
					gameObjectRenderer.material.color = whateverColor;
				}
			}
		}
		public void addNode(EdgeDoom newNode)
		{
			this.nextNodes.Add (newNode);
		}
		public void disableDoom()
		{
			this.codeBoss = 0;
			Color whateverColor = new Color(1,1,1,1);
			if (this.gameObject) {
				MeshRenderer gameObjectRenderer = this.gameObject.GetComponent<MeshRenderer> ();
				gameObjectRenderer.material.color = whateverColor;
			}
		}
	}

	public class MapDoom
	{
		private int totalBoss = 0;
		private Dictionary<Pair<int,int>,NodeDoom> nodeData;
		private Vector3 playerPosition;
		private Vector3 cameraRotation;
		private Vector3 zombiRigPosition;
		public MapDoom ()
		{
			this.nodeData = new Dictionary<Pair<int,int>,NodeDoom> ();
			this.playerPosition = new Vector3 ();
			this.cameraRotation = new Vector3 ();
			this.zombiRigPosition = new Vector3 ();
		}
		public void clear()
		{
			foreach(KeyValuePair<Pair<int,int>,NodeDoom> node in nodeData)
			{
				GameObject.Destroy (node.Value.gameObject);
			}
			nodeData.Clear ();
		}
		private NodeDoom getNode(int i, int j)
		{
			NodeDoom node = null;
			nodeData.TryGetValue(Pair<int,int>.get(i,j),out node);
			return node;
		}
		private Pair<int,int> getPos(int i, int j)
		{
			return Pair<int,int>.get (i, j);
		}
		private void CreateData(float level, int fromX,int toX,int fromY,int toY)
		{
			for (int i = fromX; i <= toX; ++i) {
				for (int j = fromY; j <= toY; ++j) {
					if(getNode(i,j)==null)
						nodeData [getPos(i,j)] = new NodeDoom (level, i, j,0);
				}
			}
		}
		public void CreateMap01()
		{
			playerPosition.Set (20.5f, 2.5f, 20.5f);
			zombiRigPosition.Set (21.0f, 2.5f, 21.0f);
			cameraRotation.Set (0.0f, 0.0f, 0.0f);
			totalBoss = 4;
			this.clear ();
			// Plataforma 01 del Inicio
			CreateData(1.0f,16,24,16,24);
			//Pasillo 01
			CreateData(1.0f,16,18,12,16);
			//Pasillo 02
			CreateData(1.0f,10,16,12,14);
			//Pasillo 03
			createBoss(1.0f,9,11,1);
			CreateData(1.0f,8,10,12,16);
			//BOSS 01
			CreateData(1.0f,-6,10,-6,10);
			//Plataforma 02
			CreateData(1.0f,6,10,16,28);
			//Pasillo 04
			CreateData(1.0f,10,26,26,28);
			//Pasillo 05
			CreateData(1.0f,24,26,22,26);
			//Pasillo 06
			CreateData(1.0f,14,16,28,32);
			//Pasillo 07
			CreateData(1.0f,12,18,32,34);
			//Pasillo 08
			createBoss(1.0f,11,35,2);
			CreateData(1.0f,12,24,34,36);
			//BOSS 02
			CreateData(1.0f,-6,10,30,46);
			//Pasillo 09
			CreateData(1.0f,26,30,24,26);
			//Pasillo 10
			CreateData(1.0f,24,32,16,18);
			//Pasillo 11
			CreateData(1.0f,30,40,14,16);
			//Pasillo 12
			CreateData(1.0f,30,32,6,14);
			//BOSS 03
			CreateData(1.0f,34,50,-8,8);
			//Pasillo 13
			createBoss(1.0f,33,7,3);
			//Pasillo 14
			CreateData(1.0f,38,40,16,22);
			//Pasillo 15
			CreateData(1.0f,30,32,28,32);
			//Pasillo 16
			CreateData(1.0f,20,22,36,42);
			//Plataforma 03
			CreateData(1.0f,30,40,22,28);
			//Plataforma 04
			CreateData(1.0f,28,34,32,38);
			//Plataforma 05
			CreateData(1.0f,34,40,46,50);
			//Pasillo 17
			CreateData(1.0f,16,30,44,46);
			//Pasillo 18
			CreateData(1.0f,28,30,38,44);
			//Pasillo 19
			CreateData(1.0f,34,40,36,38);
			//Pasillo 20
			CreateData(1.0f,38,40,38,46);
			//Pasillo 21
			createBoss(1.0f,23,51,4);
			CreateData(1.0f,22,24,46,50);
			//BOSS 04
			CreateData(1.0f,22,38,52,68);
			//WALLS
			CreateData(5.0f,-10,69,-10,69);
		}
		private void createBoss(float level, int posX, int posY, int bossID)
		{
			nodeData [getPos(posX,posY)] = new NodeDoom (level, posX, posY,bossID);
		}
		public Vector3 getPosition()
		{
			return playerPosition;
		}
		public Vector3 getRotation()
		{
			return cameraRotation;
		}
		public Vector3 getZombiRigPosition()
		{
			return zombiRigPosition;
		}
		public void disableCodeBoss(int i, int j)
		{
			NodeDoom theNode = getNode (i, j);
			if (theNode!=null) {
				if(theNode.codeBoss!=0)
				{
					theNode.disableDoom ();
					--totalBoss;
				}
			}
		}
		public bool finish()
		{
			return totalBoss == 0;
		}
	}
}
