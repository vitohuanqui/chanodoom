﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public enum TileType {
	Floor = 1,
	Forest = 5,
	Wall = System.Int32.MaxValue
}

public class Location {

	public readonly int x, y;

	public Location(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Location(float x, float y) {
		this.x = Mathf.FloorToInt(x);
		this.y = Mathf.FloorToInt(y);
	}

	public Location(Vector3 position) {
		this.x = Mathf.RoundToInt(position.x);
		this.y = Mathf.RoundToInt(position.y);
	}

	public Vector3 vector3() {
		return new Vector3 (this.x, this.y, 0f);
	}

	public override bool Equals(System.Object obj) {
		Location loc = obj as Location;
		return this.x == loc.x && this.y == loc.y;
	}

	// This is creating collisions. How do I solve this?
	public override int GetHashCode() {
		return (x * 597) ^ (y * 1173);
	}
}

public class SquareGrid {
	public static readonly Location[] DIRS = new [] {
		new Location(1, 0), // to right of tile
		new Location(0, -1), // below tile
		new Location(-1, 0), // to left of tile
		new Location(0, 1), // above tile
		new Location(1, 1), // diagonal top right
		new Location(-1, 1), // diagonal top left
		new Location(1, -1), // diagonal bottom right
		new Location(-1, -1) // diagonal bottom left
	};
	public TileType[,] tiles;
	public SquareGrid(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		TileType[,] tiles = new TileType[this.width,this.height];
	}

	public int x, y, width, height;

	public bool InBounds(Location id) {
		return (x <= id.x) && (id.x < width) && (y <= id.y) && (id.y < height);
	}
	public bool Passable(Location id) {
		return (int)tiles[id.x,id.y] < System.Int32.MaxValue;
	}

	public float Cost(Location a, Location b) {
		if (AStarSearch.Heuristic(a,b) == 2f) {
			return (float)(int)tiles[b.x,b.y] * Mathf.Sqrt(2f);
		}
		return (float)(int)tiles[b.x,b.y];
	}

	public IEnumerable<Location> Neighbors(Location id) {
		foreach (var dir in DIRS) {
			Location next = new Location(id.x + dir.x, id.y + dir.y);
			if (InBounds(next) && Passable(next)) {
				yield return next;
			}
		}
	}
}

public class PriorityQueue<T> {
	private List<KeyValuePair<T, float>> elements = new List<KeyValuePair<T, float>>();

	public int Count {
		get { return elements.Count; }
	}

	public void Enqueue(T item, float priority) {
		elements.Add(new KeyValuePair<T, float>(item,priority));
	}
	public T Dequeue() {
		int bestIndex = 0;

		for (int i = 0; i < elements.Count; i++) {
			if (elements[i].Value < elements[bestIndex].Value) {
				bestIndex = i;
			}
		}

		T bestItem = elements[bestIndex].Key;
		elements.RemoveAt(bestIndex);
		return bestItem;
	}
}

public class AStarSearch {
	public Dictionary<Location, Location> cameFrom = new Dictionary<Location, Location>();
	public Dictionary<Location, float> costSoFar = new Dictionary<Location, float>();

	private Location start;
	private Location goal;

	static public float Heuristic(Location a, Location b) {
		return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
	}

	public AStarSearch(SquareGrid graph, Location start, Location goal) {
		this.start = start;
		this.goal = goal;
		var frontier = new PriorityQueue<Location>();
		frontier.Enqueue(start, 0f);

		cameFrom.Add(start, start); // is set to start, None in example
		costSoFar.Add(start, 0f);

		while (frontier.Count > 0f) {
			Location current = frontier.Dequeue();
			if (current.Equals(goal)) break;
			foreach (var neighbor in graph.Neighbors(current)) {
				float newCost = costSoFar[current] + graph.Cost(current, neighbor);
				if (!costSoFar.ContainsKey(neighbor) || newCost < costSoFar[neighbor]) {
					if (costSoFar.ContainsKey(neighbor)) {
						costSoFar.Remove(neighbor);
						cameFrom.Remove(neighbor);
					}
					costSoFar.Add(neighbor, newCost);
					cameFrom.Add(neighbor, current);
					float priority = newCost + Heuristic(neighbor, goal);
					frontier.Enqueue(neighbor, priority);
				}
			}
		}

	}

	public List<Location> FindPath() {
		List<Location> path = new List<Location>();
		Location current = goal;
		while (!current.Equals(start)) {
			if (!cameFrom.ContainsKey(current)) {
				MonoBehaviour.print("cameFrom does not contain current.");
				return new List<Location>();
			}
			path.Add(current);
			current = cameFrom[current];
		}
		path.Reverse();
		return path;
	}
}