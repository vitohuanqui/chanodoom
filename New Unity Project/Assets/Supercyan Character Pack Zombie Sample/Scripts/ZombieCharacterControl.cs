﻿using UnityEngine; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using System.Collections.Generic;

public class ZombieCharacterControl : MonoBehaviour
{
    private enum ControlMode
    {
        Tank,
        Direct
    }

	public GameObject target;
	public NavMeshAgent agent;
    [SerializeField] private float m_moveSpeed = 2;
    [SerializeField] private float m_turnSpeed = 200;

    [SerializeField] private Animator m_animator;
	[SerializeField] private Rigidbody m_rigidBody;

    [SerializeField] private ControlMode m_controlMode = ControlMode.Tank;

    private float m_currentV = 0;
    private float m_currentH = 0;

    private readonly float m_interpolation = 10;

    private Vector3 m_currentDirection = Vector3.zero;


	bool IsLookingATObject()
	{
		//direction of the target as a vector
		float dist = Vector3.Distance(transform.position, target.transform.position);
		if (dist > 20) {
			return false;
		}
		Vector3 direction = transform.position - target.transform.position;

		float ang = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

		//current angle in degrees
		float anglecurrent = transform.eulerAngles.z;
		float checkAngle = 0;

		//Checking to see what quadrant current angle is at
		if (anglecurrent > 0 && anglecurrent <= 90)
		{
			checkAngle = ang - 90;
		}
		if (anglecurrent > 90 && anglecurrent <= 360)
		{
			checkAngle = ang + 270;
		}

		//If current angle is equal to the angle that I need to be at to look at the object, return true
		//It is possible to do "if (checkAngle == anglecurrent)" but some times you don't get an exact match so do something like below to have some error range.

		if (anglecurrent<= checkAngle+0.5f && anglecurrent >= checkAngle - 0.5f)
		{
			return true;  
		}
		else
			return false;
	}
    void Awake()
    {
        if(!m_animator) { gameObject.GetComponent<Animator>(); }
        if(!m_rigidBody) { gameObject.GetComponent<Animator>(); }
    }

	void FixedUpdate ()
	{
        switch(m_controlMode)
        {
            case ControlMode.Direct:
                DirectUpdate();
                break;

            case ControlMode.Tank:
                TankUpdate();
                break;

            default:
                Debug.LogError("Unsupported state");
                break;
        }
    }

    private void TankUpdate()
	{

		if (IsLookingATObject()){
			agent.SetDestination (target.transform.position);
			m_animator.SetTrigger("Attack");
		}
        float v = 0.5f;
        float h = 0.5f;

        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        transform.position += transform.forward * m_currentV * m_moveSpeed * Time.deltaTime;
        transform.Rotate(0, m_currentH * m_turnSpeed * Time.deltaTime, 0);
		//m_animator.SetFloat("MoveSpeed", m_currentV);
    }

    private void DirectUpdate()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        Transform camera = Camera.main.transform;

        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        Vector3 direction = camera.forward * m_currentV + camera.right * m_currentH;

        float directionLength = direction.magnitude;
        direction.y = 0;
        direction = direction.normalized * directionLength;

        if(direction != Vector3.zero)
        {
            m_currentDirection = Vector3.Slerp(m_currentDirection, direction, Time.deltaTime * m_interpolation);

            transform.rotation = Quaternion.LookRotation(m_currentDirection);
            transform.position += m_currentDirection * m_moveSpeed * Time.deltaTime;

            m_animator.SetFloat("MoveSpeed", direction.magnitude);
        }
    }
}
