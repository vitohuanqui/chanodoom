﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyController : MonoBehaviour {
	public GameObject target;
	public NavMeshAgent agent;
	public float health = 100.0f;
	private bool is_dead = false;

	bool IsLookingATObject()
	{
		//direction of the target as a vector
		float dist = Vector3.Distance(transform.position, target.transform.position);
		if (dist > 30) {
			return false;
		}
		Vector3 direction = transform.position - target.transform.position;

		float ang = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

		//current angle in degrees
		float anglecurrent = transform.eulerAngles.z;
		float checkAngle = 0;

		//Checking to see what quadrant current angle is at
		if (anglecurrent > 0 && anglecurrent <= 90)
		{
			checkAngle = ang - 90;
		}
		if (anglecurrent > 90 && anglecurrent <= 360)
		{
			checkAngle = ang + 270;
		}

		//If current angle is equal to the angle that I need to be at to look at the object, return true
		//It is possible to do "if (checkAngle == anglecurrent)" but some times you don't get an exact match so do something like below to have some error range.

		if (anglecurrent<= checkAngle+0.5f && anglecurrent >= checkAngle - 0.5f)
		{
			return true;  
		}
		else
			return false;
	}
	void Update () {
		if (IsLookingATObject()){
			agent.SetDestination (target.transform.position);
		}
		if (health < 0) {
			gameObject.SetActive (false);
		}
	}
}
